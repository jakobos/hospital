import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestRemoveMethod {

    private Department department;
    private Patient patient;
    private Employee employee;

    @BeforeEach
     public void setUp() {
        //Adds a department, patient and employee
        department = new Department("Test Department");
        patient = new Patient("A", "B", "1");
        employee = new Employee("C", "D", "2");
        //Adds the patient and employee to the department.
        department.addPatient(patient);
        department.addEmployee(employee);
     }
     @Test
     public void testRemoveMethod() {
         //Tests if the patient and employee from the department can be removed.
         assertDoesNotThrow(() -> department.remove(patient));
         assertDoesNotThrow(() -> department.remove(employee));
         System.out.println("------------------------------");
         //Tests if the department can remove the non-existing patient and employee.
         assertThrows(RemoveException.class, () -> department.remove(patient));
         assertThrows(RemoveException.class, () -> department.remove(employee));
         System.out.println("------------------------------");
         //Error-handling.
         try{
             department.remove(patient);
             throw new IllegalStateException();
         } catch(RemoveException r) {
             System.out.println(r.getMessage());
         }
         try{
             department.remove(employee);
             throw new IllegalStateException();
         } catch(RemoveException r) {
             System.out.println(r.getMessage());
         }
     }
}
