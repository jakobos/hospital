/**
 * A client-class to test the Hospital-Application.
 *
 * @author Jakob Østby
 */

public class HospitalClient {

    /**
     * Main.
     */
    public static void main(String[] args) throws RemoveException {

        Hospital hospital = new Hospital("Bærum sykehus");
        HospitalTestData.fillRegisterWithTestData(hospital);

        //Lists all the departments in the hospital.
        for(Department department : hospital.getDepartments()) {
            System.out.println(department.toString());
            System.out.println("-------------------------------");
        }

        //Lists all the employees in the 1. department.
        for(Employee employee : hospital.getDepartments().get(0).getEmployees().values()) {
            System.out.println(employee.toString());
            System.out.println("-------------------------------");
        }
        //Creates a real employee to test if the remove method will recognize that the employee does in fact belong to the department.
        Employee realEmployee = new Nurse("Ove", "Ralt", "6");
        //Removes the real employee.
        hospital.getDepartments().get(0).remove(realEmployee);
        System.out.println("-------------------------------");

        //Creates a fake patient to test if the remove method will recognize that the patient does not belong in the department.
        Patient fakePatient = new Patient("Ingen", "Ting", "404");
        //Tries to remove the fake patient.
        try{
            hospital.getDepartments().get(1).remove(fakePatient);
            //Throws another exception if the fake patient gets removed (THIS SHOULD NOT HAPPEN).
            throw new IllegalStateException();
        } catch(RemoveException r) {
            System.out.println(r.getMessage());
        }
    }
}
