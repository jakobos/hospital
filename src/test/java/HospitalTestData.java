/**
 * A class that fills a register with test data.
 */

public final class HospitalTestData {

    //Constructor for the HospitalTestData-class.
    HospitalTestData() {
        // not called
    }
    /**
     * Fills a register with test data.
     *
     * @param hospital the hospital used for the test data.
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.addEmployee(new Employee("Odd Even", "Primtallet", "1"));
        emergency.addEmployee(new Employee("Huppasahn", "DelFinito", "2"));
        emergency.addEmployee(new Employee("Rigmor", "Mortis", "3"));
        emergency.addEmployee(new GeneralPractitioner("Inco", "Gnito", "4"));
        //The surgeon underneath will not be added, because this person is already a general practitioner (Actually the person will not be added, because they have the same SSN).
        emergency.addEmployee(new Surgeon("Inco", "Gnito", "4"));
        emergency.addEmployee(new Nurse("Nina", "Teknologi", "5"));
        emergency.addEmployee(new Nurse("Ove", "Ralt", "6"));
        emergency.addPatient(new Patient("Inga", "Lykke", "7"));
        emergency.addPatient(new Patient("Ulrik", "Smål", "8"));
        hospital.getDepartments().add(emergency);

        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.addEmployee(new Employee("Salti", "Kaffen", "1"));
        childrenPolyclinic.addEmployee(new Employee("Nidel V.", "Elvefølger", "2"));
        childrenPolyclinic.addEmployee(new Employee("Anton", "Nym", "3"));
        childrenPolyclinic.addEmployee(new GeneralPractitioner("Gene", "Sis", "4"));
        childrenPolyclinic.addEmployee(new Surgeon("Nanna", "Na", "5"));
        childrenPolyclinic.addEmployee(new Nurse("Nora", "Toriet", "6"));
        childrenPolyclinic.addPatient(new Patient("Hans", "Omvar", "7"));
        childrenPolyclinic.addPatient(new Patient("Laila", "La", "8"));
        childrenPolyclinic.addPatient(new Patient("Jøran", "Drebli", "9"));
        hospital.getDepartments().add(childrenPolyclinic);
    }
}