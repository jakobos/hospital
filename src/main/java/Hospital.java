import java.util.ArrayList;

/**
 * A class that describes a hospital.
 * It also lists departments which belongs to the hospital.
 *
 * @author Jakob Østby
 */

public class Hospital {

    //Local variables:
    private String hospitalName;
    private ArrayList<Department> departments = new ArrayList<>();

    //Constructor for the Hospital-class.
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    /**
     * Returns the name of the hospital.
     *
     * @return the name of the hospital.
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * Returns a list of departments which belongs to the hospital.
     *
     * @return a list of departments which belongs to the hospital.
     */
    public ArrayList<Department> getDepartments() {
        return departments;
    }

    /**
     * Adds a new department to the hospital if the department does not already belong to the hospital.
     *
     * @param newDepartment the new department to the hospital.
     */
    public void addDepartment(Department newDepartment) {
        boolean usedName = false;
        //Checks if the department already exists.
        for(Department department : departments) {
            if(newDepartment.getDepartmentName().equals(department.getDepartmentName()))
                usedName = true;
        }

        if(usedName)
            System.out.println("The department \"" + newDepartment.getDepartmentName() + "\" already belongs to the hospital.");
        else
            departments.add(newDepartment);
    }

    /**
     * Returns all the information about the hospital.
     *
     * @return all the information about the hospital.
     */
    @Override
    public String toString() {
        return "Name: " + getHospitalName() + "\nRole: Hospital";
    }
}
