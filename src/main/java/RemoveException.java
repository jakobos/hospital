/**
 * An exception-class
 *
 * @author Jakob Østby
 */

public class RemoveException extends Exception {

    //Local variables:
    private static final long serialVersionUID = 1L;

    //Constructor of the RemoveException-class.
    public RemoveException(String message) {
        super(message);
    }
}
