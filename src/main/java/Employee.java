/**
 * A sub-class of Person. Contains the same information as a Person, but is not abstract.
 *
 * @author Jakob Østby
 */

public class Employee extends Person{

    //Constructor for the Employee-class.
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Returns all the information about the employee.
     *
     * @return all the information about the employee.
     */
    @Override
    public String toString() {
        return "Name: " + getFullName() + "\nSSN:  " + getSocialSecurityNumber() + "\nRole: Employee";
    }
}
