/**
 * A sub-class of Doctor. Contains the same information as a Doctor, but is not abstract.
 *
 * @author Jakob Østby
 */

public class Surgeon extends Doctor{

    //Constructor for the GeneralPractitioner-class.
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Sets a diagnosis to a Patient.
     *
     * @param patient the patient that gets the diagnosis.
     * @param diagnosis the diagnosis the patient will get.
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        ((Diagnosable) patient).setDiagnosis(this, diagnosis);
    }

    /**
     * Returns all the information about the surgeon.
     *
     * @return all the information about the surgeon.
     */
    @Override
    public String toString() {
        return "Name: " + getFullName() + "\nSSN:  " + getSocialSecurityNumber() + "\nRole: Surgeon";
    }
}
