/**
 * An abstract sub-class of Employee. Unlike an Employee, this class can set a diagnosis.
 *
 * @author Jakob Østby
 */

public abstract class Doctor extends Employee{

    //Constructor for the Doctor-class.
    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Sets a diagnosis to a Patient.
     *
     * @param patient the patient that gets the diagnosis.
     * @param diagnosis the diagnosis the patient will get.
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
