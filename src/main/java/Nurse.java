/**
 * A sub-class of Employee. Contains the same information as an Employee.
 *
 * @author Jakob Østby
 */

public class Nurse extends Employee{

    //Constructor for the Nurse-class.
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Returns all the information about the nurse.
     *
     * @return all the information about the nurse.
     */
    @Override
    public String toString() {
        return "Name: " + getFullName() + "\nSSN:  " + getSocialSecurityNumber() + "\nRole: Nurse";
    }
}
