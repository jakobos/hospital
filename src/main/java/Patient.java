/**
 * A sub-class of Person. Unlike a Person, this class can get a diagnosis and is not abstract.
 * It also contains an interface called Diagnosable.
 *
 * @author Jakob Østby
 */

interface Diagnosable {
    void setDiagnosis(Person diagnostician, String diagnosis);
}

public class Patient extends Person implements Diagnosable{

    //Local variables:
    private String diagnosis = "";

    //Constructor of the Patient-class.
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Returns the patient's diagnosis.
     *
     * @return the patient's diagnosis.
     */
    protected String getDiagnosis() {
        return diagnosis;
    }

    /**
     * Sets the patient's diagnosis.
     *
     * @param diagnosis the patient's diagnosis.
     */
    @Override
    public void setDiagnosis(Person diagnostician, String diagnosis) {
        //Only doctors can set a diagnosis on a patient.
        if(diagnostician instanceof Doctor)
            this.diagnosis = diagnosis;
        else
            System.out.println("Only employees of doctor can set a diagnosis on a patient.");
    }

    /**
     * Returns all the information about the patient.
     *
     * @return all the information about the patient.
     */
    @Override
    public String toString() {
        if(diagnosis.equals(""))
            return "Name: " + getFullName() + "\nSSN:  " + getSocialSecurityNumber() + "\nRole: Patient" + "\nDgns: No diagnosis";
        //Else:
        return "Name: " + getFullName() + "\nSSN:  " + getSocialSecurityNumber() + "\nRole: Patient" + "\nDgns: " + getDiagnosis();
    }
}
