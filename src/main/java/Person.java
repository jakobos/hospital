import java.util.Objects;

/**
 * An abstract class that describes a person's:
 * - First name
 * - Last name
 * - Social security number
 *
 * @author Jakob Østby
 */

public abstract class Person {

    //Local variables:
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    //Constructor for the Person-class.
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Returns the person's first name.
     *
     * @return the person's first name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name of the person.
     *
     * @param firstName the person's first name.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Returns the person's last name.
     *
     * @return the person's last name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name of the person.
     *
     * @param lastName the person's last name.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Returns the person's social security number.
     *
     * @return the person's social security number.
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Sets the social security number of the person.
     *
     * @param socialSecurityNumber the person's social security number.
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Returns the person's full name (lastName, firstName).
     *
     * @return the person's full name (lastName, firstName).
     */
    public String getFullName() {
        return lastName + ", " + firstName;
    }

    /**
     * Returns all the information about the person.
     *
     * @return all the information about the person.
     */
    @Override
    public abstract String toString();

    /**
     * Returns true or false as normal, but also returns true if the object is of same structure as a Person.
     *
     * @param object the object to compare to Person.
     * @return true or false as normal, but also returns true if the object is of same structure as a Person.
     */
    @Override
    public boolean equals(Object object) {
        if (object == this)
            return true;
        if (!(object instanceof Person))
            return false;

        Person person = (Person) object;
        return Objects.equals(firstName, person.firstName) &&
                Objects.equals(lastName, person.lastName) &&
                Objects.equals(socialSecurityNumber, person.socialSecurityNumber);
    }

    /**
     * Returns an int of the person's variables.
     *
     * @return an Integer of the person's variables.
     */
    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, socialSecurityNumber);
    }
}
