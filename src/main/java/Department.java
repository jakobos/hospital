import java.util.HashMap;
import java.util.Objects;

/**
 * A class that describes a department in a hospital.
 * It also lists employees and patients that stays in the department.
 *
 * @author Jakob Østby
 */

public class Department {

    //Local variables:
    private String departmentName;
    private HashMap<String, Employee> employees = new HashMap<>();
    private HashMap<String, Patient> patients = new HashMap<>();

    //Constructor for the Department-class:
    public Department(String departmentName) {
        //Sets departments with null to "null".
        this.departmentName = Objects.requireNonNullElse(departmentName, "null");
    }

    /**
     * Returns the name of the department.
     *
     * @return the name of the department.
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Sets the name of the department.
     *
     * @param departmentName the name of the department.
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Returns a HashMap of employees.
     *
     * @return a HashMap of employees.
     */
    public HashMap<String, Employee> getEmployees() {
        return employees;
    }

    /**
     * Adds a new employee to the department if their social security number is not already in use.
     *
     * @param newEmployee the new employee for the department.
     */
    public void addEmployee(Employee newEmployee) {
        boolean validSsn = true;

        //Looks through employees to check if the new employee's social security number is not in use.
        for(Employee employee : employees.values()) {
            if(newEmployee.getSocialSecurityNumber().equals(employee.getSocialSecurityNumber()))
                validSsn = false;
        }
        //Looks through patients to check if the new employee's social security number is not in use.
        for(Patient patient : patients.values()) {
            if(newEmployee.getSocialSecurityNumber().equals(patient.getSocialSecurityNumber()))
                validSsn = false;
        }

        if(validSsn)
            this.employees.put(newEmployee.getSocialSecurityNumber(), newEmployee);
        else
            System.out.println(newEmployee.getLastName() + "'s social security number \"" + newEmployee.getSocialSecurityNumber() + "\" is already in use by another employee or patient.");
    }

    /**
     * Returns a HashMap of patients.
     *
     * @return a HashMap of patients.
     */
    public HashMap<String, Patient> getPatients() {
        return patients;
    }

    /**
     * Adds a new patient to the department if their social security number is not already in use.
     *
     * @param newPatient the new patient for the department.
     */
    public void addPatient(Patient newPatient) {
        boolean validSsn = true;

        //Looks through employees to check if the new patient's social security number is not in use.
        for(Employee employee : employees.values()) {
            if(newPatient.getSocialSecurityNumber().equals(employee.getSocialSecurityNumber()))
                validSsn = false;
        }
        //Looks through patients to check if the new patient's social security number is not in use.
        for(Patient patient : patients.values()) {
            if(newPatient.getSocialSecurityNumber().equals(patient.getSocialSecurityNumber()))
                validSsn = false;
        }

        if(validSsn)
            this.patients.put(newPatient.getSocialSecurityNumber(), newPatient);
        else
            System.out.println(newPatient.getLastName() + "'s social security number \"" + newPatient.getSocialSecurityNumber() + "\" is already in use by another employee or patient.");
    }

    /**
     * Removes an employee or a patient if the person's information matches.
     *
     * @param person the person that will be removed from the department.
     */
    public void remove(Person person) throws RemoveException {
        boolean doesNotExist = true;

        //Checks if the information to the person matches the employee and removes the employee if true.
        for (Employee employee : employees.values()) {
            if (person.equals(employee)) {
                employees.remove(employee.getSocialSecurityNumber());
                doesNotExist = false;
            }
        }
        //Checks if the information to the person matches the patient and removes the patient if true.
        for (Patient patient : patients.values()) {
            if (person.equals(patient)) {
                patients.remove(patient.getSocialSecurityNumber());
                doesNotExist = false;
            }
        }

        //The person does not exist in either registers.
        if (doesNotExist) {
            System.out.println(person.toString() + "\nDoes not exist in the " + getDepartmentName() + " department.");
            throw new RemoveException("RemoveException.");
        }
        else
            System.out.println(person.toString() + "\nHas been removed from the " + getDepartmentName() + " department.");
    }

    /**
     * Returns all the information about the department.
     *
     * @return all the information about the department.
     */
    @Override
    public String toString() {
        return "Name: " + getDepartmentName() + "\nRole: Department";
    }
}
